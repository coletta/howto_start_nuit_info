# howto_start_nuit_info (for newbies)


## Déployer un site web dédié à l'IUT. 

1. Choisissez un des participant pour heberger votre site. Dans la suite nous nommerons son login `leader` 

2. Ce participant se log `ssh leader@162.38.222.93 -p 6666`
puis il créé le sous-répertoire  `~/public_html/NuitInfo` (ou tout autre nom).

Il donne les droits au serveur web à ce dossier `setfacl -R -m u:www-data:rwx,d:u:www-data:r-x  NuitInfo`

Enfin, il donne les droits à une autre membre de login  `etud2` de son groupe. 

`setfacl -m u:etud2:x ~/`
`setfacl -m u:etud2:rwx,d:etud2:rwx ~/public_html/NuitInfo`

Source: https://iutdepinfo.iutmontp.univ-montp2.fr/intranet/hebergement-sites/

3. Désormais `etud2` peut se connecter via FTP  `ftpinfo.iutmontp.univ-montp2.fr` port 22 (login/passwd ceux de etud2) avec le logiciel Filezilla par exemple. 

Puis il va dans le répertoire `~leader/public_html/NuitInfo`

4. Créez une page `echo.php` avec le contenu suivant  dans
   `~/public_html/NuitInfo/echo.php`.

   ```php
   <!DOCTYPE html>
   <html>
       <head>
           <meta charset="utf-8" />
           <title> Mon premier php </title>
       </head>
   
       <body>
           Voici le résultat du script PHP : 
           <?php
             // Ceci est un commentaire PHP sur une ligne
             /* Ceci est le 2ème type de commentaire PHP
             sur plusieurs lignes */
           
             // On met la chaine de caractères "hello" dans la variable 'texte'
             // Les noms de variable commencent par $ en PHP
             $texte = "hello world !";

             // On écrit le contenu de la variable 'texte' dans la page Web
             echo $texte;
             echo date();
           ?>
       </body>
   </html> 
   ```

Source: [https://romainlebreton.github.io/R3.01-DeveloppementWeb/tutorials/tutorial1.html]

5. Avec un navigateur web, visitez la page [https://webinfo.iutmontp.univ-montp2.fr/~leader/NuitInfo/echo.php]
 (où leader est toujours le login du leader ).

## Créer une base de données à l'IUT ##

Accès phpmyadmin par url : [PhpMyAdmin](http://webinfo.iutmontp.univ-montp2.fr/my)

Login : login_du_leader

mot de passe : son code INE (penser à le changer et vous le partager) .


### Créer une table:
	- soit dans l'onglet SQL :

```
CREATE TABLE `users` (
  `login` varchar(32) CHARACTER SET utf8 NOT NULL,
  `nom` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `prenom` varchar(32) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`login`, `nom`, `prenom`) VALUES
('chollet', 'Chollet', 'Antoine'),
('coletta', 'Coletta', 'Rémi'),
('hisler', 'Hisler', 'Gaelle'),
('poupet', 'Poupet', 'victor'),
('valicov', 'Valicov', 'Petru');
COMMIT;
```

	- soit visuelement avec 'Nouvelle table'

**Remarque** Pour importer des données d'un fichier CSV existant: 
	- Créer une table temporaire avec le même nombre/types de colonnes. 
	- Utiliser l'onglet importer -> choisssez CSV. 


## Lister le contenu d'une table avec PHP

** Attention** les exemples qui suivent constituent une version très simpliste 
à destination des étudiants qui n'auraient jamais accedé à une base SQL avec du PHP. 

Une méthode plus propre, notament avec l'architecture MVC est disponible sur la page du cours Web A2 [https://romainlebreton.github.io/R3.01-DeveloppementWeb/tutorials/tutorial2.html](https://romainlebreton.github.io/R3.01-DeveloppementWeb/tutorials/tutorial2.html) 

### Afficher toutes les lignes d'une table

```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>

<?php
try
{
    //je crée une connection à la base
    $db = new PDO('mysql:host=webinfo.iutmontp.univ-montp2.fr;dbname=login_leader;charset=utf8', 'login_leader', 'passwd_mySQL_leader');
    //je prépare une requete
    $req = $db->prepare('SELECT * FROM users');
    //je l'execute
    $req->execute();
    //la variable tuples récupère toutes les lignes
    $tuples = $req->fetchAll();
}
catch (Exception $e)
{
        //S'il y a une erreur , on l'affiche
        die('Erreur : ' . $e->getMessage());
}

//j'itère sur les tuples résultats
foreach ($tuples as $t) {
    //j'affiche l'attribut login du tuple
    echo '<p>'.$t['login'];
    //je génère un lien vers la page de détail
    echo '    <a href="getUser.php?login='.$t['login'].'">Detail</a>   </p>' ;

}
?>
    </body>
</html>
```

Tester cette page 
https://webinfo.iutmontp.univ-montp2.fr/~leader/getAllUsers.php





### Afficher le détail d'une ligne

```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>

<?php
//je récupère la valeur de login dans l'url site/getUser.php?login=bob
$login = $_GET['login'];

try
{
    //je crée une connection à la base
    $db = new PDO('mysql:host=webinfo.iutmontp.univ-montp2.fr;dbname=login_leader;charset=utf8', 'login_leader', 'passwd_mySQL_leader');
    //je prépare une requete
    $req = $db->prepare('SELECT * FROM users WHERE login = :login ');
    //j'execute la requete en lui passant un paramètre
    $req->execute(['login' => $login]);
    $t = $req->fetch();
}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}
    echo '<p>'.$t['login'] . ' ' . $t['prenom'] . ' ' . $t['nom'] . ' </p>' ;
?>
    </body>
</html>

```

Tester cette page en cliquant sur un des liens de la page:
https://webinfo.iutmontp.univ-montp2.fr/~leader/getAllUsers.php



### Chercher un utilisateur à partir d'une partie de son nom ou de son prenom

Une page avec le formulaire de recherche:

```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    // en action la page PHP qui recevra la requête
<form action="findUser.php" method="get">
  <label>Nom ou prenom :
<input type="text" placeholder="Ex␣:␣georges" name="motif"
id="motif" required/>

  </label>
  <button>Chercher</button>
</form>

    </body>
</html>

```

La page avec le formulaire de recherche:


```
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>

<?php
$motif = '%'.$_GET['motif'].'%';

try
{
    //je crée une connection à la base
   $db = new PDO('mysql:host=webinfo.iutmontp.univ-montp2.fr;dbname=login_leader;charset=utf8', 'login_leader', 'passwd_mySQL_leader');
    //je prépare une requete
    $req = $db->prepare('SELECT * FROM users WHERE nom LIKE :motif OR prenom LIKE :motif' );
    //je l'execute
    $req->execute(['motif' => $motif]);
    //la variable tuples récupère toutes les lignes
    $tuples = $req->fetchAll();
}
catch (Exception $e)
{
        //S'il y a une erreur , on l'affiche
        die('Erreur : ' . $e->getMessage());
}

//j'itère sur les tuples résultats
foreach ($tuples as $t) {
    //j'affiche l'attribut login du tuple
    echo '<p>'.$t['login'] .'  '.$t['nom'].'  '. $t['prenom'].'</p>' ;

}
?>
    </body>
</html>

```

## Installer un serveur Apache sur chaque poste

Pour develloper en local sur votre machine, vous pouvez déployer un environnement Apache/MySQL/PHP sur le poste de chaque participant 

**Installation :**

* sous Linux : 

  *  [XAMP](https://openclassrooms.com/courses/concevez-votre-site-web-avec-php-et-mysql/preparer-son-environnement-de-travail#/id/r-4443743)

    Vérifiez que vous installez bien aussi `phpmyadmin` et que vous activez le
    module Apache `userdir` pour pouvoir mettre vos pages Web dans `public_html`.
  
  <!-- Penser à activer PHP pour les userdir dans php*.conf -->
  
* sous Mac OS X & Windows (MAMP) :  
  [MAMP](https://openclassrooms.com/fr/courses/918836-concevez-votre-site-web-avec-php-et-mysql/4237816-preparez-votre-environnement-de-travail#/id/r-4443661)  



**Attention**, pensez à modifier le `php.ini` pour mettre `display_errors = On`
et `error_reporting = E_ALL`, pour avoir les messages d'erreurs. Car par défaut,
le serveur est configuré en mode production (`display_errors = Off`). Il faut
redémarrer Apache pour que les modifications soient prises en compte.

<!-- Si ça ne marche pas, c'est que l'on édite pas le bon php.ini . Afficher la
configuration vec phpinfo() pour trouver le php.ini qui est utilisé -->



## Utiliser Git

#### Création d'un compte GitLab

https://gitlabinfo.iutmontp.univ-montp2.fr/valicov/tutoGit1ereAnnee


